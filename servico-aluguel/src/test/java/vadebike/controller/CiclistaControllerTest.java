package vadebike.controller;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import vadebike.banco.BancoDeDados;
import vadebike.dominio.*;
import vadebike.dominio.dto.CiclistaCorpoApi;
import vadebike.dominio.dto.RequisicaoAluguel;
import vadebike.dominio.dto.RequisicaoDevolucao;
import vadebike.dominio.dto.RequisicaoPostBicicleta;
import vadebike.dominio.dto.RequisicaoPostTotem;
import vadebike.dominio.dto.RequisicaoPostTranca;
import vadebike.repositorio.CiclistaRepositorio;
import vadebike.servico.ServicoEquipamento;
import vadebike.servico.ServicoExterno;
import vadebike.util.JavalinApp;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static vadebike.modelos.CiclistaModelos.*;
import static vadebike.modelos.CiclistaModelos.getCiclista;

class CiclistaControllerTest {

    private final static JavalinApp app = new JavalinApp();
    private static final String BASE_URL = "http://localhost:7010";
    private final ServicoEquipamento servicoEquipamento = new ServicoEquipamento();
    private String idBicicleta;
    private String idTranca;
    private String idTotem;

    private final CiclistaRepositorio ciclistaRepositorio = new CiclistaRepositorio(new ServicoExterno(), new ServicoEquipamento());

    @BeforeAll
    static void init() {
        app.start(7010);
    }
    @BeforeEach
    void setUp() {
        idTotem = servicoEquipamento.salvarTotem(criarRequisicaoTotem());
        idTranca = servicoEquipamento.salvarTranca(criarRequisicaoTranca());
        idBicicleta = servicoEquipamento.salvarBicicleta(criarRequisicaoBicicleta());
        servicoEquipamento.integrarTrancaNaRede(idTranca, idTotem);
        servicoEquipamento.integrarBicicletaDaRede(idBicicleta, idTranca);
        servicoEquipamento.mudarStatusTranca(idTranca, "OCUPADA");
        servicoEquipamento.mudarStatusBicicleta(idBicicleta, "DISPONIVEL");
        BancoDeDados.carregarDados();
    }

    @AfterEach
    void rollback() {
        servicoEquipamento.removerBicicletaDaRede(idBicicleta, idTranca);
        servicoEquipamento.mudarStatusBicicleta(idBicicleta, "APOSENTADA");
        servicoEquipamento.removerTrancaDaRede(idTranca, idTotem);
        servicoEquipamento.mudarStatusTranca(idTranca, "APOSENTADA");
        servicoEquipamento.deletarBicicleta(idBicicleta);
        servicoEquipamento.deletarTranca(idTranca);
        servicoEquipamento.deletarTotem(idTotem);

    }

    @AfterAll
    static void tearDown() {
        BancoDeDados.limpar();
        app.stop();
    }

    @Test
    void alugarBicicleta_RetornaEmprestimo_QuandoBemSucedido() {
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        requisicaoAluguel.setTrancaInicio(idTranca);
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/aluguel")
                .body(requisicaoAluguel)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(201, response.getStatus());
        assertEquals(requisicaoAluguel.getCiclista(), jsonResponse.get("ciclista"));
        assertEquals(requisicaoAluguel.getTrancaInicio(), jsonResponse.get("trancaInicio"));
        assertEquals(idBicicleta, jsonResponse.get("bicicletaId"));
        assertNotNull(jsonResponse.get("cobranca"));
        assertNotNull(jsonResponse.get("horaInicio"));

        servicoEquipamento.integrarBicicletaDaRede(idBicicleta, idTranca);
        servicoEquipamento.mudarStatusBicicleta(idBicicleta, "DISPONIVEL");
        servicoEquipamento.mudarStatusTranca(idTranca, "OCUPADA");
    }

    @Test
    void devolverBicicleta_RetornaEmprestimo_QuandoBemSucedido() {
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        requisicaoAluguel.setTrancaInicio(idTranca);
        Emprestimo emprestimo = ciclistaRepositorio.alugar(requisicaoAluguel);

        RequisicaoDevolucao requisicaoDevolucao = new RequisicaoDevolucao();
        requisicaoDevolucao.setIdBicicleta(idBicicleta);
        requisicaoDevolucao.setIdTranca(idTranca);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/devolucao")
                .body(requisicaoDevolucao)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(201, response.getStatus());
        assertEquals(emprestimo.getCiclista(), jsonResponse.get("ciclista"));
        assertEquals(idTranca, jsonResponse.get("trancaInicio"));
        assertEquals(idBicicleta, jsonResponse.get("bicicletaId"));
        assertNotNull(jsonResponse.get("cobranca"));
        assertEquals(idTranca, jsonResponse.get("trancaFim"));
        assertNotNull(jsonResponse.get("horaInicio"));
        assertNotNull(jsonResponse.get("horaFim"));
    }



    @Test
    void existeEmail_RetornaVerdadeiro_QuandoExiste() {
        HttpResponse<String> response = Unirest.get(BASE_URL + "/ciclista/existeEmail/pedroclain@edu.unirio.br")
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("true", response.getBody());
    }

    @Test
    void existeEmail_RetornaFalso_QuandoNaoExiste() {
        HttpResponse<String> response = Unirest.get(BASE_URL + "/ciclista/existeEmail/xxx@email.com")
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("false", response.getBody());
    }

    @Test
    void cadastrarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getPostCiclista();
        MeioDePagamento meioDePagamento = ciclista.getMeioDePagamento();
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonCiclista = response.getBody().getObject();
        JSONObject jsonMeioDePagamento = jsonCiclista.getJSONObject("meioDePagamento");

        assertEquals(201, response.getStatus());
        assertEquals(ciclista.getEmail(), jsonCiclista.get("email"));
        assertEquals(ciclista.getNacionalidade(), jsonCiclista.get("nacionalidade"));
        assertEquals(formatarData("yyyy-MM-dd", ciclista.getNascimento()), jsonCiclista.get("nascimento"));
        assertEquals(ciclista.getCpf(), jsonCiclista.get("cpf"));
        assertEquals(ciclista.getNome(), jsonCiclista.get("nome"));
        assertEquals(false, jsonCiclista.get("status"));
        assertEquals(ciclista.getSenha(), jsonCiclista.get("senha"));

        assertEquals(meioDePagamento.getCvv(), jsonMeioDePagamento.get("cvv"));
        assertEquals(formatarData("yyyy-MM", meioDePagamento.getValidade()), jsonMeioDePagamento.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonMeioDePagamento.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonMeioDePagamento.get("nomeTitular"));

    }


    @Test
    void cadastrarCiclista_LancaExcecao_QuandoEstrangeiroSemPassaporte() {
        Ciclista ciclista = getPostCiclista();
        ciclista.setId(null);
        ciclista.setNacionalidade("USA");
        ciclista.setCpf(null);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(422, response.getStatus());
        assertEquals("O campo 'passaporte' é obrigatório para usuários estrangeiros", jsonResponse.get("mensagem"));
    }

    @Test
    void cadastrarCiclista_LancaExcecao_QuandoBrasileiroSemCpf() {
        Ciclista ciclista = getPostCiclista();
        ciclista.setId(null);
        ciclista.setCpf(null);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(422, response.getStatus());
        assertEquals("O campo 'cpf' é obrigatório para usuários brasileiros", jsonResponse.get("mensagem"));
    }

    @Test
    void atualizarCiclista_RetornaCiclista_QuandoBemSucedido() {
        CiclistaCorpoApi ciclistaCorpoApi = getCiclistaCorpoApi();
        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/ciclista/123")
                .body(ciclistaCorpoApi)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(ciclistaCorpoApi.getEmail(), jsonResponse.get("email"));
        assertEquals(ciclistaCorpoApi.getNacionalidade(), jsonResponse.get("nacionalidade"));
        assertEquals(formatarData("yyyy-MM-dd", ciclistaCorpoApi.getNascimento()), jsonResponse.get("nascimento"));
        assertEquals(ciclistaCorpoApi.getCpf(), jsonResponse.get("cpf"));
        assertEquals(ciclistaCorpoApi.getNome(), jsonResponse.get("nome"));
        assertEquals(ciclistaCorpoApi.getStatus(), jsonResponse.get("status"));
    }

    @Test
    void buscarCiclista_LancaExcecao_QuandoNaoEncontrado() {
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/000")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(404, response.getStatus());
        assertEquals("Ciclista não encontrado", jsonResponse.get("mensagem"));
    }

    @Test
    void buscarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        MeioDePagamento meioDePagamento = ciclista.getMeioDePagamento();
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/123")
                .asJson();

        JSONObject jsonCiclista = response.getBody().getObject();
        JSONObject jsonMeioDePagamento = jsonCiclista.getJSONObject("meioDePagamento");

        assertEquals(200, response.getStatus());
        assertEquals(ciclista.getEmail(), jsonCiclista.get("email"));
        assertEquals(ciclista.getNacionalidade(), jsonCiclista.get("nacionalidade"));
        assertEquals(formatarData("yyyy-MM-dd", ciclista.getNascimento()), jsonCiclista.get("nascimento"));
        assertEquals(ciclista.getCpf(), jsonCiclista.get("cpf"));
        assertEquals(ciclista.getNome(), jsonCiclista.get("nome"));
        assertEquals(ciclista.getStatus(), jsonCiclista.get("status"));
        assertEquals(ciclista.getSenha(), jsonCiclista.get("senha"));

        assertEquals(meioDePagamento.getCvv(), jsonMeioDePagamento.get("cvv"));
        assertEquals(formatarData("yyyy-MM", meioDePagamento.getValidade()), jsonMeioDePagamento.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonMeioDePagamento.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonMeioDePagamento.get("nomeTitular"));
    }

    @Test
    void buscarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido() {
        MeioDePagamento meioDePagamento = getCiclista().getMeioDePagamento();
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/cartaoDeCredito/123")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(meioDePagamento.getCvv(), jsonResponse.get("cvv"));
        assertEquals(formatarData("yyyy-MM", meioDePagamento.getValidade()), jsonResponse.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonResponse.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonResponse.get("nomeTitular"));
    }

    @Test
    void atualizarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido() {
        MeioDePagamento meioDePagamento = getCiclista().getMeioDePagamento();
        meioDePagamento.setNomeTitular("ALTERADO");
        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/cartaoDeCredito/123")
                .body(meioDePagamento)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(meioDePagamento.getCvv(), jsonResponse.get("cvv"));
        assertEquals(formatarData("yyyy-MM", meioDePagamento.getValidade()), jsonResponse.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonResponse.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonResponse.get("nomeTitular"));
    }

    @Test
    void confirmarEmail_MudaStatusParaConfirmado_QuandoBemSucedido() {
        CiclistaCorpoApi ciclistaCorpoApi = getCiclistaCorpoApi();
        ciclistaCorpoApi.setStatus(false);
        ciclistaRepositorio.atualizar(ciclistaCorpoApi.getId(), ciclistaCorpoApi);
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/123/ativar")
                .asJson();

        assertEquals(200, response.getStatus());
    }

    @Test
    void confirmarEmail_LancaExcecao_QuandoJaConfirmado() {
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/123/ativar")
                .asJson();

        assertEquals(422, response.getStatus());
        assertEquals("Esta conta já foi confirmada.", response.getBody().getObject().get("mensagem"));
    }
    @NotNull
    private String formatarData(String pattern, Date ciclista) {
        return new SimpleDateFormat(pattern).format(ciclista);
    }


    private RequisicaoPostBicicleta criarRequisicaoBicicleta() {
        RequisicaoPostBicicleta bicicleta = new RequisicaoPostBicicleta();
        bicicleta.setStatus("NOVA");
        bicicleta.setAno("1999");
        bicicleta.setMarca("Caloi");
        bicicleta.setModelo("Caloi");
        bicicleta.setNumero(123);
        return bicicleta;
    }

    private RequisicaoPostTranca criarRequisicaoTranca() {
        RequisicaoPostTranca tranca = new RequisicaoPostTranca();
        tranca.setStatus("DISPONIVEL");
        tranca.setNumero(123);
        tranca.setLocalizacao("Avenida Lúcio Costa");
        tranca.setModelo("Caloi");
        tranca.setAnoDeFabricacao("1999");
        tranca.setNumero(123);
        return tranca;
    }

    private RequisicaoPostTotem criarRequisicaoTotem() {
        RequisicaoPostTotem totem = new RequisicaoPostTotem();
        totem.setLocalizacao("Avenida Lúcio Costa");
        return totem;
    }
}
