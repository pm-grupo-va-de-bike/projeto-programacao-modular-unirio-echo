package vadebike.modelos;

import vadebike.dominio.Funcionario;

public class FuncionarioModelos {

    public static Funcionario getFuncionario() {
        Funcionario funcionario = new Funcionario();
        funcionario.setId("123");
        funcionario.setCpf("99999999999");
        funcionario.setEmail("pedroclain@edu.unirio.br");
        funcionario.setNome("123");
        funcionario.setSenha("123");
        funcionario.setFuncao("123");
        funcionario.setIdade(123);
        return funcionario;
    }
}
