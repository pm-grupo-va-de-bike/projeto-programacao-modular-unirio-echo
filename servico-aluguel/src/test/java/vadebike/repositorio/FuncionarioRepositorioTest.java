package vadebike.repositorio;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vadebike.banco.BancoDeDados;
import vadebike.dominio.Funcionario;
import vadebike.excecao.GenericApiException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static vadebike.modelos.FuncionarioModelos.getFuncionario;

class FuncionarioRepositorioTest {

    private final FuncionarioRepositorio funcionarioRepositorio = new FuncionarioRepositorio();

    @BeforeEach
    void setUp() {
        BancoDeDados.carregarDados();
    }

    @AfterAll
    static void setDown() {
        BancoDeDados.limpar();
    }

    @Test
    void cadastrarFuncionario_RetornaFuncionario_QuandoBemSucedido() {
        Funcionario funcionario = getFuncionario();
        Funcionario respostaFuncionario = funcionarioRepositorio.cadastrar(funcionario);

        assertEquals(funcionario, respostaFuncionario);
    }

    @Test
    void atualizarFuncionario_RetornaFuncionario_QuandoBemSucedido() {
        Funcionario funcionario = getFuncionario();
        funcionario.setNome("ALTERADO");
        Funcionario respostaFuncionario = funcionarioRepositorio.atualizar(funcionario.getId(), funcionario);
        assertEquals(funcionario, respostaFuncionario);
    }

    @Test
    void buscarFuncionario_LancaExcecao_QuandoNaoEncontrado() {;
        GenericApiException ex = assertThrows(GenericApiException.class,
                () -> funcionarioRepositorio.buscarFuncionario("000"));
        assertEquals(404, ex.getCodigo());
        assertEquals("Funcionario não encontrado.", ex.getMensagem());
    }

    @Test
    void buscarFuncionario_RetornaFuncionario_QuandoBemSucedido() {
        Funcionario funcionario = getFuncionario();
        Funcionario resultado = funcionarioRepositorio.buscarFuncionario(funcionario.getId());
        assertEquals(funcionario, resultado);
    }

    @Test
    void buscarFuncionarios_RetornaListaFuncionario_QuandoBemSucedido() {
        List<Funcionario> resultado = funcionarioRepositorio.buscarFuncionarios();
        assertEquals(1, resultado.size());
    }

    @Test
    void deletarFuncionario_DeletarFuncionario_QuandoBemSucedido() {
        Funcionario funcionario = getFuncionario();
        funcionarioRepositorio.deletar(funcionario.getId());
        assertThrows(GenericApiException.class, () -> funcionarioRepositorio.buscarFuncionario(funcionario.getId()));
    }

}