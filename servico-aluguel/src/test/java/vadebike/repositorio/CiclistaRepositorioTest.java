package vadebike.repositorio;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import vadebike.banco.BancoDeDados;
import vadebike.dominio.*;
import vadebike.dominio.dto.CiclistaCorpoApi;
import vadebike.dominio.dto.RequisicaoAluguel;
import vadebike.dominio.dto.RequisicaoDevolucao;
import vadebike.excecao.GenericApiException;
import vadebike.servico.ServicoEquipamento;
import vadebike.servico.ServicoExterno;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static vadebike.modelos.CiclistaModelos.*;

class CiclistaRepositorioTest {

    private CiclistaRepositorio ciclistaRepositorio;
    private ServicoEquipamento servicoEquipamentoMock;
    private ServicoExterno servicoExternoMock;

    @BeforeEach
    void setUp() {
        servicoExternoMock = Mockito.mock(ServicoExterno.class);
        servicoEquipamentoMock = Mockito.mock(ServicoEquipamento.class);
        ciclistaRepositorio = new CiclistaRepositorio(servicoExternoMock, servicoEquipamentoMock);

        when(servicoExternoMock.realizarCobranca(any())).thenReturn(getCobranca());
        doNothing().when(servicoExternoMock).enviarEmail(any());
        doNothing().when(servicoExternoMock).validarCartao(any());
        when(servicoEquipamentoMock.buscarBicicleta(any())).thenReturn(getBicicleta());
        when(servicoEquipamentoMock.buscarTranca(any())).thenReturn(getTranca());
        doNothing().when(servicoEquipamentoMock).mudarStatusTranca(any(), any());
        doNothing().when(servicoEquipamentoMock).mudarStatusBicicleta(any(), any());
        doNothing().when(servicoEquipamentoMock).integrarTrancaNaRede(any(), any());
        doNothing().when(servicoEquipamentoMock).removerTrancaDaRede(any(), any());
        doNothing().when(servicoEquipamentoMock).integrarBicicletaDaRede(any(), any());
        doNothing().when(servicoEquipamentoMock).removerBicicletaDaRede(any(), any());

        BancoDeDados.carregarDados();
    }

    @AfterAll
    static void tearDown() {
        BancoDeDados.limpar();
    }

    @Test
    void alugarBicicleta_RetornaEmprestimo_QuandoBemSucedido() {
        Bicicleta bicicleta = getBicicleta();
        Cobranca cobranca = getCobranca();
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        Emprestimo emprestimo = ciclistaRepositorio.alugar(requisicaoAluguel);

        assertEquals(requisicaoAluguel.getCiclista(), emprestimo.getCiclista());
        assertEquals(bicicleta.getId(), emprestimo.getBicicletaId());
        assertEquals(cobranca.getId(), emprestimo.getCobranca());
        assertEquals(requisicaoAluguel.getTrancaInicio(), emprestimo.getTrancaInicio());
    }
    
    @Test
    void alugarBicicleta_LancaGenericException_QuandoTrancaVazia() {
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        Tranca tranca = getTranca();
        tranca.setId("321");
        tranca.setStatus("DISPONIVEL");
        tranca.setIdBicicleta(null);
        requisicaoAluguel.setTrancaInicio("321");
        when(servicoEquipamentoMock.buscarTranca(any())).thenReturn(tranca);
        GenericApiException ex =  assertThrows(GenericApiException.class, 
            () -> ciclistaRepositorio.alugar(requisicaoAluguel));
        assertEquals(422, ex.getCodigo());
        assertEquals("Tranca vazia", ex.getMensagem());
    }

    @Test
    void alugarBicicleta_LancaGenericException_QuandoCiclistaJaPossuiEmprestimo() {
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        ciclistaRepositorio.alugar(requisicaoAluguel);

        GenericApiException ex =  assertThrows(GenericApiException.class, 
            () -> ciclistaRepositorio.alugar(requisicaoAluguel));
        assertEquals(422, ex.getCodigo());
        assertEquals("Ciclista já possui um aluguel em andamento", ex.getMensagem());
    }

    @Test
    void devolver_RetornaEmprestimo_QuandoBemSucedido() {
        Bicicleta bicicleta = getBicicleta();
        Cobranca cobranca = getCobranca();
        Ciclista ciclista = getCiclista();
        Tranca tranca = getTranca();
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        RequisicaoDevolucao requisicaoDevolucao = new RequisicaoDevolucao();

        requisicaoDevolucao.setIdBicicleta("123");
        requisicaoDevolucao.setIdTranca("123");
        ciclistaRepositorio.alugar(requisicaoAluguel);
        tranca.setStatus("DISPONIVEL");
        tranca.setIdBicicleta(null);
        bicicleta.setStatus("OCUPADA");
        when(servicoEquipamentoMock.buscarTranca(any())).thenReturn(tranca);
        when(servicoEquipamentoMock.buscarBicicleta(any())).thenReturn(bicicleta);
        Emprestimo emprestimo = ciclistaRepositorio.devolver(requisicaoDevolucao);

        assertEquals(ciclista.getId(), emprestimo.getCiclista());
        assertEquals(bicicleta.getId(), emprestimo.getBicicletaId());
        assertEquals(cobranca.getId(), emprestimo.getCobranca());
        assertEquals(requisicaoDevolucao.getIdTranca(), emprestimo.getTrancaFim());
    }

    @Test
    void devolverBicicleta_LancaGenericException_QuandoBicicletaNaoAlugada() {
        Bicicleta bicicleta = getBicicleta();
        Tranca tranca = getTranca();
        RequisicaoDevolucao requisicaoDevolucao = new RequisicaoDevolucao();
        requisicaoDevolucao.setIdBicicleta("321");
        requisicaoDevolucao.setIdTranca("123");
    
        tranca.setStatus("DISPONIVEL");
        tranca.setIdBicicleta(null);
        bicicleta.setStatus("OCUPADA");
        when(servicoEquipamentoMock.buscarTranca(any())).thenReturn(tranca);
        when(servicoEquipamentoMock.buscarBicicleta(any())).thenReturn(bicicleta);

        
        GenericApiException ex = assertThrows(GenericApiException.class, ()-> ciclistaRepositorio.devolver(requisicaoDevolucao));
        assertEquals(422, ex.getCodigo());
        assertEquals("Bicicleta não está alugada", ex.getMensagem());
    }

    @Test
    void devolverBicicleta_LancaGenericException_TrancaOcupada() {
        Bicicleta bicicleta = getBicicleta();
        Tranca tranca = getTranca();
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        RequisicaoDevolucao requisicaoDevolucao = new RequisicaoDevolucao();

        requisicaoDevolucao.setIdBicicleta("123");
        requisicaoDevolucao.setIdTranca("123");
        ciclistaRepositorio.alugar(requisicaoAluguel);
        tranca.setIdBicicleta("321");
        bicicleta.setStatus("OCUPADA");
        when(servicoEquipamentoMock.buscarTranca(any())).thenReturn(tranca);
        when(servicoEquipamentoMock.buscarBicicleta(any())).thenReturn(bicicleta);
        GenericApiException ex = assertThrows(GenericApiException.class, ()-> ciclistaRepositorio.devolver(requisicaoDevolucao));
        assertEquals(422, ex.getCodigo());
        assertEquals("Tranca não está disponível", ex.getMensagem());
    }

    @Test
    void existeEmail_RetornaVerdadeiro_QuandoExiste() {
        Ciclista ciclista = getCiclista();
        boolean resultado = ciclistaRepositorio.existeCiclistaPorEmail(ciclista.getEmail());

        assertTrue(resultado);
    }

    @Test
    void existeEmail_RetornaFalso_QuandoNaoExiste() {
        boolean resultado = ciclistaRepositorio.existeCiclistaPorEmail("000");

        assertFalse(resultado);
    }

    @Test
    void cadastrarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getPostCiclista();
        Ciclista respostaCiclista = ciclistaRepositorio.cadastrar(ciclista);

        assertNotNull(respostaCiclista.getId());
        assertEquals(ciclista.getEmail(), respostaCiclista.getEmail());
        assertEquals(ciclista.getCpf(), respostaCiclista.getCpf());
        assertEquals(ciclista.getMeioDePagamento(), respostaCiclista.getMeioDePagamento());
        assertEquals(ciclista.getNacionalidade(), respostaCiclista.getNacionalidade());
        assertEquals(ciclista.getNascimento(), respostaCiclista.getNascimento());
        assertEquals(ciclista.getSenha(), respostaCiclista.getSenha());
        assertEquals(ciclista.getPassaporte(), respostaCiclista.getPassaporte());
        assertEquals(ciclista.getStatus(), respostaCiclista.getStatus());

    }

    @Test
    void cadastrarCiclista_LancaExcecao_QuandoEstrangeiroSemPassaporte() {
        Ciclista ciclista = getPostCiclista();
        ciclista.setNacionalidade("USA");
        ciclista.setCpf(null);

        GenericApiException ex = assertThrows(GenericApiException.class,
                () -> ciclistaRepositorio.cadastrar(ciclista));
        assertEquals(422, ex.getCodigo());
        assertEquals("O campo 'passaporte' é obrigatório para usuários estrangeiros", ex.getMensagem());
    }

    @Test
    void cadastrarCiclista_LancaExcecao_QuandoBrasileiroSemCpf() {
        Ciclista ciclista = getPostCiclista();
        ciclista.setCpf(null);

        GenericApiException ex = assertThrows(GenericApiException.class,
                () -> ciclistaRepositorio.cadastrar(ciclista));
        assertEquals(422, ex.getCodigo());
        assertEquals("O campo 'cpf' é obrigatório para usuários brasileiros", ex.getMensagem());
    }

    @Test
    void atualizarCiclista_RetornaCiclista_QuandoBemSucedido() {
        CiclistaCorpoApi ciclistaCorpoApi = getCiclistaCorpoApi();
        CiclistaCorpoApi respostaCiclista = ciclistaRepositorio.atualizar(ciclistaCorpoApi.getId(), ciclistaCorpoApi);
        assertEquals(ciclistaCorpoApi, respostaCiclista);
    }

    @Test
    void buscarCiclista_LancaExcecao_QuandoNaoEncontrado() {
        GenericApiException ex = assertThrows(GenericApiException.class,
                () -> ciclistaRepositorio.buscarCiclista("000"));
        assertEquals(404, ex.getCodigo());
        assertEquals("Ciclista não encontrado", ex.getMensagem());
    }

    @Test
    void buscarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        Ciclista resultadoCiclista = ciclistaRepositorio.buscarCiclista(ciclista.getId());
        assertEquals(ciclista, resultadoCiclista);
    }

    @Test
    void buscarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        MeioDePagamento resultadoMeioDePagamento = ciclistaRepositorio.buscarCartaoPorCiclista(ciclista.getId());
        assertEquals(ciclista.getMeioDePagamento(), resultadoMeioDePagamento);
    }

    @Test
    void atualizarCartaoPorCiclista_AtualizaCartao_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        ciclista.getMeioDePagamento().setNomeTitular("ALTERADO");
        MeioDePagamento respostaMeioDePagamento = ciclistaRepositorio.atualizarCartaoPorCiclista(ciclista.getId(), ciclista.getMeioDePagamento());
        assertEquals(ciclista.getMeioDePagamento(), respostaMeioDePagamento);
    }

    @Test
    void confirmarEmail_MudaStatusParaConfirmado_QuandoBemSucedido() {
        CiclistaCorpoApi ciclistaCorpoApi = getCiclistaCorpoApi();
        ciclistaCorpoApi.setStatus(false);
        ciclistaRepositorio.atualizar(ciclistaCorpoApi.getId(), ciclistaCorpoApi);
        ciclistaRepositorio.confirmarEmail(ciclistaCorpoApi.getId());
        Ciclista respostaCiclista = ciclistaRepositorio.buscarCiclista(ciclistaCorpoApi.getId());

        assertTrue(respostaCiclista.getStatus());
    }

    @Test
    void confirmarEmail_LancaExcecao_QuandoJaConfirmado() {
        Ciclista ciclista = getCiclista();
        GenericApiException ex = assertThrows(GenericApiException.class,
                () -> ciclistaRepositorio.confirmarEmail(ciclista.getId()));
        assertEquals(422, ex.getCodigo());
        assertEquals("Esta conta já foi confirmada.", ex.getMensagem());
    }
}