package vadebike.repositorio;

import vadebike.banco.BancoDeDados;
import vadebike.dominio.*;
import vadebike.dominio.dto.CiclistaCorpoApi;
import vadebike.dominio.dto.RequisicaoAluguel;
import vadebike.dominio.dto.RequisicaoCobranca;
import vadebike.dominio.dto.RequisicaoDevolucao;
import vadebike.excecao.GenericApiException;
import vadebike.servico.ServicoEquipamento;
import vadebike.servico.ServicoExterno;
import vadebike.util.Urls;
import vadebike.util.Validador;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class CiclistaRepositorio {
    private final ServicoExterno servicoExterno;
    private final ServicoEquipamento servicoEquipamento;

    public CiclistaRepositorio(ServicoExterno servicoExterno, ServicoEquipamento servicoEquipamento) {
        this.servicoExterno = servicoExterno;
        this.servicoEquipamento = servicoEquipamento;
    }


    public Emprestimo alugar(RequisicaoAluguel requisicaoAluguel) {
        Validador.validarCampos(requisicaoAluguel);
        Ciclista ciclista = buscarCiclista(requisicaoAluguel.getCiclista());
        verificarSeExisteAluguel(ciclista);
        Tranca tranca = servicoEquipamento.buscarTranca(requisicaoAluguel.getTrancaInicio());
        if (tranca.getIdBicicleta() == null) {
            throw new GenericApiException(422, "Tranca vazia");
        }
        
        RequisicaoCobranca requisicaoCobranca = new RequisicaoCobranca();
        requisicaoCobranca.setCiclista(ciclista.getId());
        requisicaoCobranca.setValor(10.0);
        Cobranca cobranca = servicoExterno.realizarCobranca(requisicaoCobranca);
        Bicicleta bicicleta = servicoEquipamento.buscarBicicleta(tranca.getIdBicicleta());
        servicoEquipamento.removerBicicletaDaRede(bicicleta.getId(), tranca.getId());
        servicoEquipamento.mudarStatusBicicleta(bicicleta.getId(), "OCUPADA");
        servicoEquipamento.mudarStatusTranca(tranca.getId(), "DISPONIVEL");

        Emprestimo emprestimo = new Emprestimo();
        emprestimo.setId(UUID.randomUUID().toString());
        emprestimo.setCiclista(ciclista.getId());
        emprestimo.setBicicletaId(bicicleta.getId());
        emprestimo.setCobranca(cobranca.getId());
        emprestimo.setHoraInicio(new Date());
        emprestimo.setTrancaInicio(tranca.getId());
        BancoDeDados.TABELA_EMPRESTIMO.put(emprestimo.getId(), emprestimo);
        enviarEmail(ciclista.getEmail(), "Aluguel realizado com sucesso \n\n" + emprestimo);
        return emprestimo;
    }

    private void verificarSeExisteAluguel(Ciclista ciclista) {
        Optional<Emprestimo> emprestimoOptional = buscarEmprestimoAbertoPorCiclista(ciclista);
        if (emprestimoOptional.isPresent()) {
            enviarEmail(ciclista.getEmail(), "Um aluguel de bicicleta foi bloqueado pois já existe outro em " +
                    "andamento. \n\n" + emprestimoOptional.get());
            throw new GenericApiException(422, "Ciclista já possui um aluguel em andamento");
        }
    }

    private void enviarEmail(String destinatario, String mensagem) {
        Email email = new Email();
        email.setEmail(destinatario);
        email.setMensagem(mensagem);
        servicoExterno.enviarEmail(email);
    }

    public Emprestimo devolver(RequisicaoDevolucao requisicaoDevolucao) {
        Validador.validarCampos(requisicaoDevolucao);
        Bicicleta bicicleta = servicoEquipamento.buscarBicicleta(requisicaoDevolucao.getIdBicicleta());
        Tranca tranca = servicoEquipamento.buscarTranca(requisicaoDevolucao.getIdTranca());
        if (!tranca.getStatus().equals("DISPONIVEL")) throw new GenericApiException(422, "Tranca não está disponível");
        Emprestimo emprestimo = buscarEmprestimoAbertoPorBicicleta(bicicleta);
        Ciclista ciclista = buscarCiclista(emprestimo.getCiclista());
        realizarCobrancaExtra(emprestimo);
        servicoEquipamento.integrarBicicletaDaRede(bicicleta.getId(), tranca.getId());
        servicoEquipamento.mudarStatusBicicleta(bicicleta.getId(), "DISPONIVEL");
        servicoEquipamento.mudarStatusTranca(tranca.getId(), "OCUPADA");
        emprestimo.setHoraFim(new Date());
        emprestimo.setTrancaFim(tranca.getId());
        BancoDeDados.TABELA_EMPRESTIMO.replace(emprestimo.getId(), emprestimo);
        enviarEmail(ciclista.getEmail(), "Devolução realizada com sucesso \n\n" + emprestimo);
        return emprestimo;
    }

    private Emprestimo buscarEmprestimoAbertoPorBicicleta(Bicicleta bicicleta) {
        return BancoDeDados.TABELA_EMPRESTIMO.keySet()
                .stream()
                .map(BancoDeDados.TABELA_EMPRESTIMO::get)
                .filter(e -> e.getBicicletaId().equals(bicicleta.getId()) && e.getHoraFim() == null)
                .findFirst().orElseThrow(() -> new GenericApiException(422, "Bicicleta não está alugada"));
    }


    private Optional<Emprestimo> buscarEmprestimoAbertoPorCiclista(Ciclista ciclista) {
        return BancoDeDados.TABELA_EMPRESTIMO.keySet()
                .stream()
                .map(BancoDeDados.TABELA_EMPRESTIMO::get)
                .filter(e -> e.getCiclista().equals(ciclista.getId()) && e.getHoraFim() == null)
                .findFirst();
    }

    private void realizarCobrancaExtra(Emprestimo emprestimo) {
        double tempo = (new Date().getTime() - emprestimo.getHoraInicio().getTime()) / 1000.0 / 60.0; // em minutos
        if (tempo > 150.0) {
            double excesso = tempo - 120.0;
            RequisicaoCobranca requisicaoCobranca = new RequisicaoCobranca();
            requisicaoCobranca.setCiclista(emprestimo.getCiclista());
            requisicaoCobranca.setValor(5.0 * (excesso / 30.0));    // 5 reais a cada meia hora
            servicoExterno.realizarCobranca(requisicaoCobranca);
        }
    }

    public Ciclista buscarCiclista(String id) {
        Ciclista ciclista = BancoDeDados.TABELA_CICLISTA.get(id);
        if (ciclista == null) throw new GenericApiException(404, "Ciclista não encontrado");
        return ciclista;
    }

    public MeioDePagamento buscarCartaoPorCiclista(String idCiclista) {
        Ciclista ciclista = buscarCiclista(idCiclista);
        return ciclista.getMeioDePagamento();
    }

    public MeioDePagamento atualizarCartaoPorCiclista(String idCiclista, MeioDePagamento novoMeioDePagamento) {
        Validador.validarCampos(novoMeioDePagamento);
        servicoExterno.validarCartao(novoMeioDePagamento);
        Ciclista ciclista = buscarCiclista(idCiclista);
        ciclista.setMeioDePagamento(novoMeioDePagamento);
        BancoDeDados.TABELA_CICLISTA.replace(idCiclista, ciclista);
        enviarEmail(ciclista.getEmail(), "Meio de pagamento atualizado.\n\n" + novoMeioDePagamento);
        return novoMeioDePagamento;
    }

    public CiclistaCorpoApi atualizar(String id, CiclistaCorpoApi ciclistaCorpoApi) {
        Validador.validarCampos(ciclistaCorpoApi);
        Ciclista ciclista = buscarCiclista(id);
        ciclista.setCpf(ciclistaCorpoApi.getCpf());
        ciclista.setNascimento(ciclistaCorpoApi.getNascimento());
        ciclista.setNome(ciclistaCorpoApi.getNome());
        ciclista.setEmail(ciclistaCorpoApi.getEmail());
        ciclista.setNacionalidade(ciclistaCorpoApi.getNacionalidade());
        ciclista.setPassaporte(ciclistaCorpoApi.getPassaporte());
        ciclista.setStatus(ciclistaCorpoApi.getStatus());
        Validador.validarEstrangeiro(ciclista);
        BancoDeDados.TABELA_CICLISTA.put(id, ciclista);
        ciclistaCorpoApi.setId(id);
        enviarEmail(ciclista.getEmail(), "Dados atualizados com sucesso.\n\n" + ciclista);
        return ciclistaCorpoApi;
    }

    public boolean existeCiclistaPorEmail(String email) {
        return buscarPorEmail(email).isPresent();
    }

    private Optional<Ciclista> buscarPorEmail(String email) {
        return BancoDeDados.TABELA_CICLISTA.keySet()
                .stream()
                .map(BancoDeDados.TABELA_CICLISTA::get)
                .filter((c) -> c.getEmail().equals(email))
                .findFirst();
    }

    public Ciclista cadastrar(Ciclista ciclista) {
        Validador.validarCampos(ciclista);
        if (buscarPorEmail(ciclista.getEmail()).isPresent()) throw new GenericApiException(422, "Email já cadastrado");
        Validador.validarEstrangeiro(ciclista);
        servicoExterno.validarCartao(ciclista.getMeioDePagamento());
        String id = UUID.randomUUID().toString();
        ciclista.setId(id);
        ciclista.setStatus(false);
        BancoDeDados.TABELA_CICLISTA.put(id, ciclista);
        String mensagem = String.format("Obrigado por usar nossos serviços. Por favor, confirme seu cadastramento " +
        "clicando no link\n\n %s/ciclista/%s/ativar", Urls.ALUGUEL_BASE_URL, ciclista.getId());
        enviarEmail(ciclista.getEmail(), mensagem);
        return ciclista;
    }

    public CiclistaCorpoApi confirmarEmail(String id) {
        Ciclista ciclista = buscarCiclista(id);
        if (ciclista.getStatus()) throw new GenericApiException(422, "Esta conta já foi confirmada.");
        ciclista.setStatus(true);
        BancoDeDados.TABELA_CICLISTA.replace(id, ciclista);
        CiclistaCorpoApi ciclistaCorpoApi = new CiclistaCorpoApi();
        ciclistaCorpoApi.setId(ciclista.getId());
        ciclistaCorpoApi.setEmail(ciclista.getEmail());
        ciclistaCorpoApi.setCpf(ciclista.getCpf());
        ciclistaCorpoApi.setNome(ciclista.getNome());
        ciclistaCorpoApi.setNacionalidade(ciclista.getNacionalidade());
        ciclistaCorpoApi.setStatus(ciclista.getStatus());
        ciclistaCorpoApi.setPassaporte(ciclista.getPassaporte());
        return ciclistaCorpoApi;
    }
}
