package vadebike.repositorio;

import vadebike.banco.BancoDeDados;
import vadebike.dominio.Funcionario;
import vadebike.excecao.GenericApiException;
import vadebike.util.Validador;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class FuncionarioRepositorio {

    public Funcionario buscarFuncionario(String id) {
        Funcionario funcionario = BancoDeDados.TABELA_FUNCIONARIO.get(id);
        if (funcionario == null) throw new GenericApiException(404, "Funcionario não encontrado.");
        return funcionario;
    }

    public List<Funcionario> buscarFuncionarios() {
        return BancoDeDados.TABELA_FUNCIONARIO.keySet()
                .stream()
                .map(BancoDeDados.TABELA_FUNCIONARIO::get)
                .collect(Collectors.toList());
    }

    public Funcionario atualizar(String id, Funcionario novoFuncionario) {
        Validador.validarCampos(novoFuncionario);
        buscarFuncionario(id);
        novoFuncionario.setId(id);
        BancoDeDados.TABELA_FUNCIONARIO.put(id, novoFuncionario);
        return novoFuncionario;
    }

    public void deletar(String id) {
        buscarFuncionario(id);
        BancoDeDados.TABELA_FUNCIONARIO.remove(id);
    }

    public Funcionario cadastrar(Funcionario funcionario) {
        Validador.validarCampos(funcionario);
        funcionario.setId(UUID.randomUUID().toString());
        BancoDeDados.TABELA_FUNCIONARIO.put(funcionario.getId(), funcionario);
        return funcionario;
    }
}
