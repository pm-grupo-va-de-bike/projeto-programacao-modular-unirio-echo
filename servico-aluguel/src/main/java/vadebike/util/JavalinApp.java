package vadebike.util;

import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJson;
import vadebike.controller.CiclistaController;
import vadebike.controller.FuncionarioController;
import vadebike.excecao.GenericApiException;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    private final Javalin app =
            Javalin.create(config -> config.defaultContentType = "application/json")
                    .routes(() -> {
                        path("/aluguel", () -> post(CiclistaController::alugar));
                        path("/devolucao", () -> post(CiclistaController::devolver));

                        path("/ciclista", () -> post(CiclistaController::salvarCiclista));
                        path("/ciclista/:idCiclista", () -> get(CiclistaController::buscarCiclista));
                        path("/ciclista/:idCiclista", () -> put(CiclistaController::atualizarCiclista));
                        path("/ciclista/:idCiclista/ativar", () -> get(CiclistaController::confirmarEmail));
                        path("/ciclista/existeEmail/:email", () -> get(CiclistaController::existeEmail));

                        path("/cartaoDeCredito/:idCiclista", () -> get(CiclistaController::buscarCartao));
                        path("/cartaoDeCredito/:idCiclista", () -> put(CiclistaController::atualizarCartao));

                        path("/funcionario", () -> get(FuncionarioController::buscarFuncionarios));
                        path("/funcionario", () -> post(FuncionarioController::salvarFuncionario));
                        path("/funcionario/:idFuncionario", () -> get(FuncionarioController::buscarFuncionario));
                        path("/funcionario/:idFuncionario", () -> delete(FuncionarioController::deletarFuncionario));
                        path("/funcionario/:idFuncionario", () -> put(FuncionarioController::atualizarFuncionario));

                    }).error(500, ctx -> {
                        GenericApiException ex = new GenericApiException(500, "Ocorreu um erro durante a operação");
                        ctx.result(JavalinJson.toJson(ex));
                    }).error(404, ctx -> {
                        if (ctx.attribute("handle") == null) {
                            GenericApiException ex = new GenericApiException(404, "Página não encontrada");
                            ctx.result(JavalinJson.toJson(ex));
                        }
                    })
                    .exception(GenericApiException.class, (e, ctx) -> {
                        ctx.attribute("handle", true);
                        String response = JavalinJson.getToJsonMapper().map(e);
                        ctx.status(e.getCodigo());
                        ctx.result(response);
                    });


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
