package vadebike.util;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;
import vadebike.dominio.Ciclista;
import vadebike.excecao.GenericApiException;

import java.util.Set;

public class Validador {
    private static final Validator validator;

    static {
        validator = Validation
                .byDefaultProvider()
                .configure()
                .messageInterpolator(new ParameterMessageInterpolator())
                .buildValidatorFactory()
                .getValidator();
    }
    public static void validarCampos(Object instancia) {
        verificarConstraints(validator.validate(instancia));
    }

    public static void validarEstrangeiro(Ciclista ciclista) {
        if(!ciclista.getNacionalidade().toUpperCase().startsWith("BR")) {
            if (ciclista.getPassaporte() == null) throw new GenericApiException(422, "O campo 'passaporte' é obrigatório para usuários estrangeiros");
            verificarConstraints(validator.validate(ciclista.getPassaporte()));

        } else {
            if (ciclista.getCpf() == null) throw new GenericApiException(422, "O campo 'cpf' é obrigatório para usuários brasileiros");
            // TODO Regex para validar CPF
        }
    }

    private static void verificarConstraints(Set<ConstraintViolation<Object>> constraintViolations) {
        if (!constraintViolations.isEmpty()) {
            throw new GenericApiException(422, constraintViolations.iterator().next().getMessage());
        }
    }
}
