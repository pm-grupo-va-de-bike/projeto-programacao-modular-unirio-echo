package vadebike.util;

public class Urls {
    public static final String EQUIPAMENTO_BASE_URL = "https://va-de-bike-equipamento.herokuapp.com";
    public static final String EXTERNO_BASE_URL = "https://va-de-bike-externo.herokuapp.com";
    public static final String ALUGUEL_BASE_URL = "https://va-de-bike-aluguel.herokuapp.com";
}
