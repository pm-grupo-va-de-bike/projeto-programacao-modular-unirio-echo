package vadebike.servico;

import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import vadebike.dominio.*;
import vadebike.dominio.dto.RequisicaoCobranca;
import vadebike.excecao.GenericApiException;

import static vadebike.util.Urls.EXTERNO_BASE_URL;

public class ServicoExterno {
    public void validarCartao(MeioDePagamento meioDePagamento) {
        HttpResponse<String> response = Unirest.post(EXTERNO_BASE_URL + "/validarCartaoDeCredito")
                .body(meioDePagamento)
                .asString();
        validarResposta(response);
    }

    public Cobranca realizarCobranca(RequisicaoCobranca requisicaoCobranca) {
        HttpResponse<String> response = Unirest.post(EXTERNO_BASE_URL + "/cobranca")
                .body(requisicaoCobranca)
                .asString();
        validarResposta(response);

        return JavalinJson.fromJson(response.getBody(), Cobranca.class);
    }

    public void enviarEmail(Email email) {
        HttpResponse<String> response = Unirest.post(EXTERNO_BASE_URL + "/enviarEmail")
                .body(email)
                .asString();
        validarResposta(response);
    }

    private void validarResposta(HttpResponse<String> response) {
        if (response.getStatus() != 200 && response.getStatus() != 201) {
            throw JavalinJson.fromJson(response.getBody(), GenericApiException.class);
        }
    }
}
