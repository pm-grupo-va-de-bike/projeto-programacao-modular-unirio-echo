package vadebike.servico;

import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import vadebike.dominio.*;
import vadebike.dominio.dto.RequisicaoBicicletaNaRede;
import vadebike.dominio.dto.RequisicaoPostBicicleta;
import vadebike.dominio.dto.RequisicaoPostTotem;
import vadebike.dominio.dto.RequisicaoPostTranca;
import vadebike.dominio.dto.RequisicaoTrancaNaRede;
import vadebike.excecao.GenericApiException;

import static vadebike.util.Urls.EQUIPAMENTO_BASE_URL;

public class ServicoEquipamento {
    public void mudarStatusBicicleta(String id, String status){
        HttpResponse<String> response = Unirest.post(EQUIPAMENTO_BASE_URL + "/bicicleta/" + id + "/status/" + status)
                .body("")
                .asString();
        validarResposta(response);
    }

    public void mudarStatusTranca(String id, String status){
        HttpResponse<String> response = Unirest.post(EQUIPAMENTO_BASE_URL + "/tranca/" + id + "/status/" + status)
                .body("")
                .asString();
        validarResposta(response);
    }

    public void integrarTrancaNaRede(String idTranca, String idTotem){
        RequisicaoTrancaNaRede requisicaoTrancaNaRede = new RequisicaoTrancaNaRede(idTranca, idTotem);
        HttpResponse<String> response = Unirest.post(EQUIPAMENTO_BASE_URL + "/tranca/integrarNaRede")
                .body(requisicaoTrancaNaRede)
                .asString();
        validarResposta(response);
    }

    public void removerTrancaDaRede(String idTranca, String idTotem){
        RequisicaoTrancaNaRede requisicaoTrancaNaRede = new RequisicaoTrancaNaRede(idTranca, idTotem);
        HttpResponse<String> response = Unirest.post(EQUIPAMENTO_BASE_URL + "/tranca/removerDaRede")
                .body(requisicaoTrancaNaRede)
                .asString();
        validarResposta(response);
    }

    public void integrarBicicletaDaRede(String idBicicleta, String idTranca){
        RequisicaoBicicletaNaRede requisicaoBicicletaNaRede = new RequisicaoBicicletaNaRede(idBicicleta, idTranca);
        HttpResponse<String> response = Unirest.post(EQUIPAMENTO_BASE_URL + "/bicicleta/integrarNaRede")
                .body(requisicaoBicicletaNaRede)
                .asString();
        validarResposta(response);
    }

    public void removerBicicletaDaRede(String idBicicleta, String idTranca){
        RequisicaoBicicletaNaRede requisicaoBicicletaNaRede = new RequisicaoBicicletaNaRede(idBicicleta, idTranca);
        HttpResponse<String> response = Unirest.post(EQUIPAMENTO_BASE_URL + "/bicicleta/removerDaRede")
                .body(requisicaoBicicletaNaRede)
                .asString();
        validarResposta(response);
    }

    public Tranca buscarTranca(String id) {
        HttpResponse<String> response = Unirest.get(EQUIPAMENTO_BASE_URL + "/tranca/" + id)
                .asString();
        validarResposta(response);

        return JavalinJson.fromJson(response.getBody(), Tranca.class);
    }

    public Bicicleta buscarBicicleta(String id) {
        HttpResponse<String> response = Unirest.get(EQUIPAMENTO_BASE_URL + "/bicicleta/" + id)
                .asString();
        validarResposta(response);

        return JavalinJson.fromJson(response.getBody(), Bicicleta.class);
    }

    public String salvarBicicleta(RequisicaoPostBicicleta requisicaoPostBicicleta) {
        return (String) Unirest.post(EQUIPAMENTO_BASE_URL + "/bicicleta")
                .body(requisicaoPostBicicleta)
                .asJson()
                .getBody()
                .getObject()
                .get("id");
    }

    public String salvarTranca(RequisicaoPostTranca requisicaoPostTranca) {
        return (String) Unirest.post(EQUIPAMENTO_BASE_URL + "/tranca")
                .body(requisicaoPostTranca)
                .asJson()
                .getBody()
                .getObject()
                .get("id");
    }

    public String salvarTotem(RequisicaoPostTotem requisicaoPostTotem) {
        return (String) Unirest.post(EQUIPAMENTO_BASE_URL + "/totem")
                .body(requisicaoPostTotem)
                .asJson()
                .getBody()
                .getObject()
                .get("id");
    }

    public void deletarBicicleta(String id) {
        Unirest.delete(EQUIPAMENTO_BASE_URL +"/bicicleta/"+id);
    }

    public void deletarTranca(String id) {
        Unirest.delete(EQUIPAMENTO_BASE_URL +"/tranca/"+id);
    }

    public void deletarTotem(String id) {
        Unirest.delete(EQUIPAMENTO_BASE_URL +"/totem/"+id);
    }

    private void validarResposta(HttpResponse<String> response) {
        if (response.getStatus() != 200 && response.getStatus() != 201) {
            throw JavalinJson.fromJson(response.getBody(), GenericApiException.class);
        }
    }
}
