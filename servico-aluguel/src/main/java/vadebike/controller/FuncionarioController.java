package vadebike.controller;

import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;
import vadebike.dominio.Funcionario;
import vadebike.repositorio.FuncionarioRepositorio;

import java.util.List;

public class FuncionarioController {

    public static FuncionarioRepositorio funcionarioRepositorio = new FuncionarioRepositorio();

    private FuncionarioController(){}

    public static void salvarFuncionario(Context ctx) {
        String body = ctx.body();
        Funcionario postFuncionario = JavalinJson.getFromJsonMapper().map(body, Funcionario.class);
        Funcionario funcionario = funcionarioRepositorio.cadastrar(postFuncionario);
        String response = JavalinJson.toJson(funcionario);
        ctx.result(response);
        ctx.status(201);
    }

    public static void buscarFuncionarios(Context ctx) {
        List<Funcionario> funcionarioList = funcionarioRepositorio.buscarFuncionarios();
        String response = JavalinJson.toJson(funcionarioList);
        ctx.result(response);
        ctx.status(200);
    }

    public static void buscarFuncionario(Context ctx) {
        String id = ctx.pathParam("idFuncionario");
        Funcionario resposta = funcionarioRepositorio.buscarFuncionario(id);
        ctx.status(200);
        ctx.result(JavalinJson.toJson(resposta));
    }

    public static void atualizarFuncionario(Context ctx) {
        String body = ctx.body();
        String id = ctx.pathParam("idFuncionario");
        Funcionario funcionario = JavalinJson.getFromJsonMapper().map(body, Funcionario.class);
        Funcionario response = funcionarioRepositorio.atualizar(id, funcionario);
        ctx.status(200);
        ctx.result(JavalinJson.toJson(response));
    }

    public static void deletarFuncionario(Context ctx) {
        String id = ctx.pathParam("idFuncionario");
        funcionarioRepositorio.deletar(id);
        ctx.status(200);
    }
}
