package vadebike.controller;

import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;
import vadebike.dominio.*;
import vadebike.dominio.dto.CiclistaCorpoApi;
import vadebike.dominio.dto.RequisicaoAluguel;
import vadebike.dominio.dto.RequisicaoDevolucao;
import vadebike.repositorio.CiclistaRepositorio;
import vadebike.servico.ServicoEquipamento;
import vadebike.servico.ServicoExterno;


public class CiclistaController {
    public static CiclistaRepositorio ciclistaRepositorio = new CiclistaRepositorio(new ServicoExterno(), new ServicoEquipamento());

    private CiclistaController(){}

    public static void alugar(Context ctx) {
        String body = ctx.body();
        RequisicaoAluguel requisicaoAluguel = JavalinJson.getFromJsonMapper().map(body, RequisicaoAluguel.class);
        final Emprestimo emprestimo = ciclistaRepositorio.alugar(requisicaoAluguel);
        String response = JavalinJson.toJson(emprestimo);
        ctx.result(response);
        ctx.status(201);
    }

    public static void devolver(Context ctx) {
        String body = ctx.body();
        RequisicaoDevolucao requisicaoDevolucao = JavalinJson.getFromJsonMapper().map(body, RequisicaoDevolucao.class);
        Emprestimo emprestimo = ciclistaRepositorio.devolver(requisicaoDevolucao);
        String response = JavalinJson.toJson(emprestimo);
        ctx.result(response);
        ctx.status(201);
    }

    public static void salvarCiclista(Context ctx) {
        String body = ctx.body();
        Ciclista postCiclista = JavalinJson.getFromJsonMapper().map(body, Ciclista.class);
        Ciclista ciclista = ciclistaRepositorio.cadastrar(postCiclista);
        String response = JavalinJson.toJson(ciclista);
        ctx.result(response);
        ctx.status(201);
    }

    public static void buscarCiclista(Context ctx) {
        String ciclistaId = ctx.pathParam("idCiclista");
        Ciclista ciclista = ciclistaRepositorio.buscarCiclista(ciclistaId);
        String response = JavalinJson.toJson(ciclista);
        ctx.result(response);
        ctx.status(200);
    }

    public static void atualizarCiclista(Context ctx) {
        String body = ctx.body();
        String id = ctx.pathParam("idCiclista");
        CiclistaCorpoApi ciclistaCorpoApi = JavalinJson.getFromJsonMapper().map(body, CiclistaCorpoApi.class);
        CiclistaCorpoApi resultado = ciclistaRepositorio.atualizar(id, ciclistaCorpoApi);
        ctx.status(200);
        ctx.result(JavalinJson.toJson(resultado));

    }

    public static void existeEmail(Context ctx) {
        String email = ctx.pathParam("email");
        boolean existe = ciclistaRepositorio.existeCiclistaPorEmail(email);
        ctx.status(200);
        ctx.result(String.valueOf(existe));
    }

    public static void confirmarEmail(Context ctx) {
        String id = ctx.pathParam("idCiclista");
        CiclistaCorpoApi resultado = ciclistaRepositorio.confirmarEmail(id);
        ctx.status(200);
        ctx.result(JavalinJson.toJson(resultado));

    }

    public static void buscarCartao(Context ctx) {
        String id = ctx.pathParam("idCiclista");
        MeioDePagamento resultado = ciclistaRepositorio.buscarCartaoPorCiclista(id);
        ctx.status(200);
        ctx.result(JavalinJson.toJson(resultado));
    }

    public static void atualizarCartao(Context ctx) {
        String body = ctx.body();
        String id = ctx.pathParam("idCiclista");
        MeioDePagamento meioDePagamento = JavalinJson.getFromJsonMapper().map(body, MeioDePagamento.class);
        MeioDePagamento resultado = ciclistaRepositorio.atualizarCartaoPorCiclista(id, meioDePagamento);
        ctx.status(200);
        ctx.result(JavalinJson.toJson(resultado));
    }
}
