package vadebike.servico;

import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import vadebike.dominio.Ciclista;
import vadebike.excecoes.GenericApiException;

public class ServicoAluguel {
    public static final String BASE_URL = "https://va-de-bike-aluguel.herokuapp.com";

    public Ciclista getCiclista(String id) {
        HttpResponse<String> response = Unirest.get(BASE_URL + "/ciclista/" + id)
                .asString();
        validarResposta(response);
        return JavalinJson.fromJson(response.getBody(), Ciclista.class);
    }

    private void validarResposta(HttpResponse<String> response) {
        if (response.getStatus() != 200 && response.getStatus() != 201) {
            throw JavalinJson.fromJson(response.getBody(), GenericApiException.class);
        }
    }
}
