package vadebike.servico;

import org.junit.jupiter.api.Test;
import vadebike.dominio.RequisicaoEmail;

import static org.junit.jupiter.api.Assertions.*;

public class ServicoEmailTest {

    private final ServicoEmail servicoEmail = new ServicoEmail();

    @Test
    void enviaEmail_QuandoBemSucedido() {
        RequisicaoEmail requisicaoEmail = createEmail();
        assertDoesNotThrow(() -> servicoEmail.enviarMensagem(requisicaoEmail));
    }

    RequisicaoEmail createEmail() {
        RequisicaoEmail requisicaoEmail = new RequisicaoEmail();
        requisicaoEmail.setEmail("pedroclain@edu.unirio.br");
        requisicaoEmail.setMensagem("Testando");
        return requisicaoEmail;
    }
}
