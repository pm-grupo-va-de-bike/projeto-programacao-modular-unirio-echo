package vadebike.dominio;

public enum TrancaStatus {
    DISPONIVEL, OCUPADA, MANUTENCAO, APOSENTADA, NOVA
}
