package vadebike.util;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;
import vadebike.excecoes.GenericApiException;

import java.util.Set;

public class Validador {
    private static final Validator validator;

    static {
        validator = Validation
                .byDefaultProvider()
                .configure()
                .messageInterpolator(new ParameterMessageInterpolator())
                .buildValidatorFactory()
                .getValidator();
    }
    public static void validarCampos(Object instancia) {
        Set<ConstraintViolation<Object>> violationSet = validator.validate(instancia);
        if (!violationSet.isEmpty()) {
            throw new GenericApiException(422, violationSet.iterator().next().getMessage());
        }
    }
}
