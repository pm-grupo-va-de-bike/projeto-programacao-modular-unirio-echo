package vadebike.repositorio;

import vadebike.banco.BancoDeDados;
import vadebike.dominio.Bicicleta;
import vadebike.dominio.Totem;
import vadebike.dominio.Tranca;
import vadebike.dominio.dto.PostTotem;
import vadebike.excecoes.GenericApiException;
import vadebike.util.Validador;

import java.util.*;
import java.util.stream.Collectors;

public class TotemRepositorio {

    public Totem salvarTotem(PostTotem postTotem) {
        Validador.validarCampos(postTotem);
        Totem totem = new Totem();
        String id = UUID.randomUUID().toString();
        totem.setLocalizacao(postTotem.getLocalizacao());
        totem.setId(id);
        BancoDeDados.TOTEM_TABLE.put(id, totem);
        return totem;
    }

    public List<Totem> buscarTotens() {
        return BancoDeDados.TOTEM_TABLE.keySet()
                .stream()
                .map(BancoDeDados.TOTEM_TABLE::get)
                .collect(Collectors.toList());
    }

    public Totem buscarTotem(String id) {
        Totem totem = BancoDeDados.TOTEM_TABLE.get(id);
        if (totem == null) throw new GenericApiException(404, "Totem não encontrado");
        return totem;
    }

    public Totem atualizarTotem(String id, Totem novoTotem){
        buscarTotem(id);
        novoTotem.setId(id);
        BancoDeDados.TOTEM_TABLE.replace(id, novoTotem);
        return novoTotem;
    }

    public void deletarTotem(String id){
        if (BancoDeDados.TOTEM_TABLE.containsKey(id)){
            BancoDeDados.TOTEM_TABLE.remove(id);
        } else throw new GenericApiException(404, "Totem não encontrado");
    }

    public List<Tranca> buscarTrancas(String id) {
        return BancoDeDados.TRANCA_TABLE.keySet()
                .stream()
                .map(BancoDeDados.TRANCA_TABLE::get)
                .filter((t) -> t.getIdTotem().equals(id))
                .collect(Collectors.toList());
    }

    public List<Bicicleta> buscarBicicletas(String id){
        List<Tranca> trancas = buscarTrancas(id);

        return trancas.stream()
                .map(t -> BancoDeDados.BICICLETA_TABLE.get(t.getIdBicicleta()))
                .filter(b -> b != null)
                .collect(Collectors.toList());
    }
}
