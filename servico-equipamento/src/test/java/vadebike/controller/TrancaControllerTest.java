package vadebike.controller;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vadebike.banco.BancoDeDados;
import vadebike.dominio.Bicicleta;
import vadebike.dominio.Tranca;
import vadebike.dominio.TrancaStatus;
import vadebike.dominio.dto.BicicletaNaRede;
import vadebike.dominio.dto.PostTranca;
import vadebike.dominio.dto.TrancaNaRede;
import vadebike.modelo.BicicletaModelos;
import vadebike.repositorio.BicicletaRepositorio;
import vadebike.util.JavalinApp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static vadebike.modelo.BicicletaModelos.getBicicletaNaRede;
import static vadebike.modelo.TrancaModelos.*;

class TrancaControllerTest {

    private final static JavalinApp app = new JavalinApp();
    private static final String BASE_URL = "http://localhost:7010/tranca";
    private final BicicletaRepositorio bicicletaRepositorio = new BicicletaRepositorio();

    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void stop() {
        app.stop();
        BancoDeDados.limpar();
    }

    @BeforeEach
    void setUp() {
        BancoDeDados.carregarDados();
    }

    @Test
    void salvaTranca_QuandoBemSucedido() {
        PostTranca postTranca = getPostTranca();
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL)
                .body(postTranca)
                .asJson();
        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(201, response.getStatus());
        assertEquals(String.valueOf(postTranca.getNumero()), jsonResponse.get("numero"));
        assertEquals(postTranca.getLocalizacao(), jsonResponse.get("localizacao"));
        assertEquals(postTranca.getAnoDeFabricacao(), jsonResponse.get("anoDeFabricacao"));
        assertEquals(postTranca.getStatus().toString(), jsonResponse.get("status"));
        assertEquals(postTranca.getModelo(), jsonResponse.get("modelo"));
        assertNotNull(jsonResponse.get("id"));
    }

    @Test
    void buscarTrancas() {
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL)
                .asJson();

        assertEquals(200, response.getStatus());
        assertNotNull(response.getBody().getArray().get(0));
    }

    @Test
    void buscarTranca() {
        Tranca tranca = getTranca();
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL+"/123")
                .asJson();
        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(tranca.getNumero(), jsonResponse.get("numero"));
        assertEquals(tranca.getLocalizacao(), jsonResponse.get("localizacao"));
        assertEquals(tranca.getAnoDeFabricacao(), jsonResponse.get("anoDeFabricacao"));
        assertEquals(tranca.getStatus().toString(), jsonResponse.get("status"));
        assertEquals(tranca.getModelo(), jsonResponse.get("modelo"));
        assertEquals(tranca.getId(), jsonResponse.get("id"));
    }

    @Test
    void atualizaTranca_QuandoBemSucedido() {
        Tranca tranca = getTranca();
        tranca.setLocalizacao("ALTERADO");
        HttpResponse<JsonNode> response = Unirest.put(BASE_URL+"/123")
                .body(tranca)
                .asJson();
        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(tranca.getNumero(), jsonResponse.get("numero"));
        assertEquals(tranca.getLocalizacao(), jsonResponse.get("localizacao"));
        assertEquals(tranca.getAnoDeFabricacao(), jsonResponse.get("anoDeFabricacao"));
        assertEquals(tranca.getStatus().toString(), jsonResponse.get("status"));
        assertEquals(tranca.getModelo(), jsonResponse.get("modelo"));
        assertEquals(tranca.getId(), jsonResponse.get("id"));
    }



    @Test
    void buscarBicicletaPorTranca() {
        Bicicleta bicicleta = BicicletaModelos.getBicicleta();
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL+"/123/bicicleta")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(bicicleta.getNumero(), jsonResponse.get("numero"));
        assertEquals(bicicleta.getStatus().toString(), jsonResponse.get("status"));
        assertEquals(bicicleta.getAno(), jsonResponse.get("ano"));
        assertEquals(bicicleta.getModelo(), jsonResponse.get("modelo"));
        assertEquals(bicicleta.getId(), jsonResponse.get("id"));
    }

    @Test
    void alterarStatus() {
        Tranca tranca = getTranca();
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL+"/123/status/MANUTENCAO")
                .asJson();
        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(tranca.getNumero(), jsonResponse.get("numero"));
        assertEquals(tranca.getLocalizacao(), jsonResponse.get("localizacao"));
        assertEquals(tranca.getAnoDeFabricacao(), jsonResponse.get("anoDeFabricacao"));
        assertEquals(TrancaStatus.MANUTENCAO.toString(), jsonResponse.get("status"));
        assertEquals(tranca.getModelo(), jsonResponse.get("modelo"));
        assertEquals(tranca.getId(), jsonResponse.get("id"));
    }

    @Test
    void removeTrancaDaRede_QuandoBemSucedido() {
        TrancaNaRede trancaNaRede = getTrancaNaRede();
        bicicletaRepositorio.removerDaRede(getBicicletaNaRede());
        HttpResponse<String> response = Unirest.post(BASE_URL+"/removerDaRede")
                .body(trancaNaRede)
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("", response.getBody());
    }

    @Test
    void integraTrancaNaRede_QuandoBemSucedido() {
        TrancaNaRede trancaNaRede = getTrancaNaRede();

        HttpResponse<String> response = Unirest.post(BASE_URL+"/integrarNaRede")
                .body(trancaNaRede)
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("", response.getBody());
    }

    @Test
    void deletaTranca_QuandoBemSucedido() {
        BicicletaNaRede bicicletaNaRede = getBicicletaNaRede();
        bicicletaRepositorio.removerDaRede(bicicletaNaRede);
        HttpResponse<String> response = Unirest.delete(BASE_URL+"/123")
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("", response.getBody());
    }
}