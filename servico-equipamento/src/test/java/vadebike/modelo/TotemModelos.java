package vadebike.modelo;

import vadebike.dominio.Totem;
import vadebike.dominio.dto.PostTotem;

public class TotemModelos {

    public static PostTotem getPostTotem() {
        PostTotem postTotem = new PostTotem();
        postTotem.setLocalizacao("Avenida 123");
        return postTotem;
    }

    public static Totem getTotem() {
        Totem totem = new Totem();
        totem.setLocalizacao("123");
        totem.setId("123");
        return totem;
    }
}
